<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class UnknownRepresentationProperty extends RuntimeException
{
}
